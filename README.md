django
======

优点： 大而全，封装多种功能 开箱即用 目录结构适合中大型程序

缺点： 内含功能套多 有的用不上
### 教程： 投票应用
确认python 版本和Django 版本已安装
python -m django --version

2.新建工程项目
django-amdin startproject mysite
3.项目目录结构
根目录下 manage.py启动服务的入口
跟项目同名的mysite文件夹下是主要项目代码 有的项目中这个文件夹
又叫做APP src
setting.py 设置
URLS.py 路由
wsgi.py 打包应用部署相关 model.pyORM 相关的类
views.py 业务逻辑
4.启动项目 python manage.py runserver
5.生成应用 python manage.py startapp polls
app 是项目工程app应用是项目中的一个功能模块，polls 目录下
migration 是sql迁移脚本 admin.py 是后台插件
App.py models.py
python
### 流程
浏览器请求url -> mysite/urls.py ->
polls/urls.py -> polls/views.py -> 但会浏览器展示


### 时间
场景：国内时间是0-24 国外用户访问由于时差则差了几个小时

TIME_ZONE = 'UTC'
USE_TZ = True
不带时区的时间aware_time, 带时区的时间 local_time(本地时间)
GTM、UTC(世界调和时)。中国东8区 UTC+8
python内置的datetime包生成本地时间
如果网站只有国内访问 USE_TZ直接设置为False
时间可以由datetime包生成，存储在数据库的是不带时区的本地时间

为了避免上面提到的场景 django 的解决方案是 由time_zone生成
带随时区的时间，根据TIME_ZONE设置转换UTC时间存入数据库，HTML渲染时从数据库
取出UTC时间 根据访问者的时区转换成访问者当地的时间
如果网站多国访问，USE_TZ 设置为True

### i18n
i18n意为国际化。网站上的菜单不同国家的人访问展示不同的语言
有的翻译的配置文件


### 配置
mysite/settings
database{} engine 用那种数据库 name 数据库文件存储名

1. settings.py INSTALLED_APP 插上应用
2. python manage.py makemigrations polls
根据模块下models。py 生成迁移脚本
3. python mange.py sqlmigration polls 0001 查看sql语句
4. python manage.py migrate 执行迁移脚本

注意：orm 框架并非万能，较复杂的数据结构可能报错，可以手动在数据库建立表
不走migrate 只要保证models.py 中的定义字段属性与数据库表一致


### shell
python manage.py shell
与一般的python交互式解释器不同 还包含django 上下文环境 拥有Django相关功能

### orm



其他需要关注的配置：
INSTALLED_APPS Django数据库迁移时会检查installed apps中的应用涉及的类

路由和视图
===
1. url 部分可变 ：https://www.xxx.com/sport/page/2/
路由中数字变化表示不同页。优点：清晰，利于SEO优化

2. url 传参： https://www.xxx.com/sport/?page=2
优点、：可以传较多参数





