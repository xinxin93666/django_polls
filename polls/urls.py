from django.urls import path
from . import views
app_name = 'polls'
urlpatterns = [
    # http://ip:port/polls/
    path('', views.index, name='index'),
    # http://ip:port/polls/index/
    path('index', views.index, name='index'),
    # http://ip:port/polls/1/
    path('<int:question_id>/', views.detail, name='detail'),
    path('<int:question_id>/results/', views.results, name='results'),
    path('<int:question_id>/vote/', views.vote, name='vote'),

    path('simple/', views.SimpleView, name='simple')
]
